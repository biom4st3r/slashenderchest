package com.biom4st3r.slashenderchest;

import net.fabricmc.api.ModInitializer;
import net.minecraft.client.network.ClientDummyContainerProvider;
import net.minecraft.container.ContainerType;
import net.minecraft.container.GenericContainer;
import net.minecraft.inventory.EnderChestInventory;
import net.minecraft.server.command.ServerCommandManager;
import net.minecraft.text.TranslatableTextComponent;
import net.fabricmc.fabric.api.registry.CommandRegistry;

public class SlashEChest implements ModInitializer {
	public static final String MODID = "biom4st3r_slashenderchest";
	@Override
	public void onInitialize() {
		CommandRegistry.INSTANCE.register(false, serverCommandSourceCommandDispatcher  -> serverCommandSourceCommandDispatcher.register(
			ServerCommandManager.literal("ec").executes(context -> 
			{
				context.getSource().getPlayer().openContainer(new ClientDummyContainerProvider((int_1,playerInventory,playerEntity) -> 
				{
					EnderChestInventory eChestInv = playerEntity.getEnderChestInventory();
					eChestInv.setCurrentBlockEntity(null);
					return new GenericContainer(ContainerType.GENERIC_9X3, int_1, playerInventory, eChestInv, 3);//3, as in GENERIC_9X3
					//return new GenericContainer.Generic9x3(int_1, playerInventory, eChestInv);
					//return new GenericContainer(ContainerType.GENERIC_9X3, int_1, playerInventory, playerEntity.getEnderChestInventory(),int_1);
				}, new TranslatableTextComponent("Ender Chest")));
				return 0;
			})));
	}
}
